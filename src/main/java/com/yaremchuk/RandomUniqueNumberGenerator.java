package com.yaremchuk;

import java.util.ArrayList;
import java.util.Random;

public class RandomUniqueNumberGenerator {
    public static ArrayList<Integer> generate(int amount) {
        Random random = new Random();
        ArrayList<Integer> arrayList = new ArrayList<Integer>();

        while (arrayList.size() < amount) { // how many numbers u need - it will 6
            int a = random.nextInt(amount*10)+1; // this will give numbers between 1 and 50.

            if (!arrayList.contains(a)) {
                arrayList.add(a);
            }
        }
        return arrayList;
    }
}