package com.yaremchuk;

import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class Main {
    private static final int ARRAY_SIZE = 100000;
    private static final int K = 500;

    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<Integer> uniqueArray = generate(ARRAY_SIZE);
        System.out.println(uniqueArray);

        long start = System.currentTimeMillis();
        KthSmallestElementFinder kElementFinder = new KthSmallestElementFinder(uniqueArray,K);
        int kth = kElementFinder.find();
        long end = System.currentTimeMillis();
        System.out.println(K+"th smallest element: "+ kth);
        NumberFormat formatter = new DecimalFormat("#0.00000");
        System.out.print("Execution time is " + formatter.format((end - start) / 1000d) + " seconds\n");

        System.out.println("\nParallel computing:");
        start = System.currentTimeMillis();
        KthSmallestElementFinder parallelKthFinder = new ParallelKthSmallestElementFinder(uniqueArray,K,3);
        int parallelKth = parallelKthFinder.find();
        end = System.currentTimeMillis();
        System.out.println(K+"th smallest element: "+ kth);
        System.out.print("Execution time is " + formatter.format((end - start) / 1000d) + " seconds\n");
    }
}
