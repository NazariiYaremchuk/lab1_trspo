package com.yaremchuk;

import java.util.*;

import java.util.concurrent.*;
import java.util.stream.Collectors;

public class ParallelKthSmallestElementFinder extends KthSmallestElementFinder implements Callable<Integer> {
    private int numberOfThreads = 1;
    private ExecutorService executor;

    public ParallelKthSmallestElementFinder(ArrayList<Integer> list, int nthSmallest, int numberOfThreads) {
        super(list, nthSmallest);
        this.numberOfThreads = numberOfThreads;
        this.executor =  Executors.newFixedThreadPool(numberOfThreads);
    }
    public ParallelKthSmallestElementFinder(ArrayList<Integer> list, int nthSmallest) {
        super(list, nthSmallest);
        this.executor =  Executors.newCachedThreadPool();
    }
    @Override
    public int find(){
        Future<Integer> future = null;
        for (int begin = 0; begin < numberOfThreads; begin++) {
            if (this.numberOfThreads != 1) {
                future = executor.submit(
                        new ParallelKthSmallestElementFinder(list,nthSmallest, numberOfThreads));
            } else {
                future = executor.submit(
                        new ParallelKthSmallestElementFinder(list,nthSmallest));
            }
        }
        executor.shutdown();
        return nthSmallest;
    }
    @Override
    public Integer call() throws Exception {
        return quickSelect(list, nthSmallest);
    }
}
