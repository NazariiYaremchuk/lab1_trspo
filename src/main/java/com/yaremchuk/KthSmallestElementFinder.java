package com.yaremchuk;

import java.util.*;

public class KthSmallestElementFinder {  protected ArrayList<Integer> list;
    protected int nthSmallest;

    public KthSmallestElementFinder(ArrayList<Integer> list, int nthSmallest) {
        this.list = list;
        this.nthSmallest = nthSmallest;
    }

    protected int quickSelect(ArrayList<Integer> list, int nthSmallest){

        //Choose random number in range of 0 to array length
        Random random =  new Random();
        //This will give random number which is not greater than length - 1
        int pivotIndex = random.nextInt(list.size() - 1);

        int pivot = list.get(pivotIndex);

        ArrayList<Integer> smallerNumberList = new ArrayList<Integer>();
        ArrayList<Integer> greaterNumberList = new ArrayList<Integer>();


        for(int i=0; i<list.size(); i++){
            if(list.get(i)<pivot){
                smallerNumberList.add(list.get(i));
            }
            else if(list.get(i)>pivot){
                greaterNumberList.add(list.get(i));
            }
            else{
                //Do nothing
            }
        }
        if(nthSmallest < smallerNumberList.size()){
            return quickSelect(smallerNumberList, nthSmallest);
        }

        else if(nthSmallest > (list.size() - greaterNumberList.size())){
            nthSmallest = nthSmallest - (list.size() - greaterNumberList.size());
            return quickSelect(greaterNumberList,nthSmallest);
        }
        else{
            return pivot;
        }
    }
    public int find(){
        int kElement = quickSelect(list, nthSmallest);
        return kElement;
    }
}
